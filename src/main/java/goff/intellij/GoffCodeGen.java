package goff.intellij;

import com.intellij.notification.NotificationDisplayType;
import com.intellij.notification.NotificationGroup;
import com.intellij.notification.NotificationType;
import com.intellij.openapi.actionSystem.AnAction;
import com.intellij.openapi.actionSystem.AnActionEvent;
import com.intellij.openapi.actionSystem.DataKeys;
import com.intellij.openapi.vfs.VirtualFile;
import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import org.jetbrains.annotations.NotNull;

public class GoffCodeGen extends AnAction {

  @Override
  public void actionPerformed(@NotNull AnActionEvent e) {
    NotificationGroup noti =
        new NotificationGroup("goff.intelij3", NotificationDisplayType.BALLOON, true);

    VirtualFile file = DataKeys.VIRTUAL_FILE.getData(e.getDataContext());

    if (!file.isDirectory()) {
      String path = file.getPath();
      if (path.endsWith(".proto")) {
        String out = generateCode(path);
        if (out != null) {
          noti.createNotification("Goff Intelij", out, NotificationType.INFORMATION, null)
              .notify(e.getProject());
        }
      }
    }
  }

  public static String generateCode(String path) {

    String pattern = "/tutum/ares/";

    if (path.contains(pattern)) {
      String homeDir = path.substring(0, path.lastIndexOf(pattern) + pattern.length());

      String fileName = path.substring(path.lastIndexOf("/") + 1);

      ProcessBuilder pb = new ProcessBuilder();

      pb.command("bash", "-c", "make codegen file=" + fileName);

      pb.directory(new File(homeDir));

      Process process;
      String lines;
      try {
        process = pb.start();
        String outPut = readStream(process.getInputStream());
        String outErr = readStream(process.getErrorStream());
        lines = outPut + "\r\n" + outErr;
      } catch (IOException e) {
        lines = e.getMessage();
      }
      return lines;
    }
    return null;
  }

  private static String readStream(InputStream inputStream) {
    BufferedReader reader = null;
    String lines = "";
    try {
      reader = new BufferedReader(new InputStreamReader(inputStream));
      String line;
      while ((line = reader.readLine()) != null) {
        lines = lines + "\r\n" + line;
      }
      reader.close();
    } catch (IOException e) {
      lines = e.getMessage();
    } finally {
      try {
        if (reader != null) {
          reader.close();
        }
      } catch (Exception ex) {
        // ignore
      }
    }
    return lines;
  }
}
